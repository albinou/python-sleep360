# Bluetooth LE messages

This files gives a quick overview of GATT messages sent/received by the SleepCompanion bulb.

GATT messages are divided into 3 services :

## Hardware service

Hardware service UUID: `00ff5502-3c25-45cb-99dc-1754766b829a`

| Handle | Characteristic UUID                    | Message purpose                        | Data sent              | Data received          |
| ------ | -------------------------------------- | -------------------------------------- | ---------------------- | ---------------------- |
| 0x0018 | `044993e6-5eed-439a-9497-9e4086539756` | Power off                              | 0x00                   |                        |
| 0x001a | `054993e6-5eed-439a-9497-9e4086539756` | Send date & time                       | 0xWWYYBBDDHHMMSS       |                        |
| 0x0022 | `0b4993e6-5eed-439a-9497-9e4086539756` | Get temperature                        |                        | Array(0xTTTTTTTTCC)    |

Where:

- 0xWW is the day of the week (starting from 0 on the Monday, e.i. 1 for Tuesday)
- 0xYY is the year (only the last 2 digits, i.e. 19 for 2019)
- 0xDD is the day of the month
- 0xBB is the month of the year (starting from 1, i.e. 1 for January)
- 0xHH is the hour of the day (local time)
- 0xMM is the minutes count
- 0xSS is the seconds count
- 0xTTTTTTTT is a timestamp in seconds (in Little Endian order; timezone offset must be substracted)
- 0xCC is the temperature un Celsius (Values seem overestimated on my bulb)

## Application service

Application service UUID: `01ff5502-3c25-45cb-99dc-1754766b829a`

| Handle | Characteristic UUID                    | Message purpose                        | Data sent              | Data received          |
| ------ | -------------------------------------- | -------------------------------------- | ---------------------- | ---------------------- |
| 0x0025 | `f04993e6-5eed-439a-9497-9e4086539756` | Seems necessary to turn ON the bulb    | 0x00000100             |                        |
| 0x0027 | `f14993e6-5eed-439a-9497-9e4086539756` | Set color                              | 0x0000000000RRGGBBWWCC |                        |

Where:

- 0xRRGGBB is the color (red, green, blue)
- 0xWWCC is a warm/cold value

## Bootloader service

Bootloader service UUID: `02ff5502-3c25-45cb-99dc-1754766b829a`

| Handle | Characteristic UUID                    | Message purpose                        | Data sent              | Data received          |
| ------ | -------------------------------------- | -------------------------------------- | ---------------------- | ---------------------- |
| 0x0003 | `a04993e6-5eed-439a-9497-9e4086539756` | Get bootloader info                    |                        | 0xVVVV0000MMMMMMMM     |

Where:

- 0xVVVV is the firmware version (in Little Endian order)
- 0xMMMMMMMM is the bootloader mode ("AAAA" or "BBBB" apparently)
