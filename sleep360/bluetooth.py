from pydbus import SystemBus


from sleep360.errors import Sleep360Error


BLUEZ_BUS_NAME = "org.bluez"
BLUEZ_OBJ_PATH = "/org/bluez"
BLUEZ_ADAPTER_NAME = BLUEZ_BUS_NAME + ".Adapter1"
BLUEZ_DEVICE_NAME = BLUEZ_BUS_NAME + ".Device1"
BLUEZ_GATT_SERVICE_NAME = BLUEZ_BUS_NAME + ".GattService1"
BLUEZ_GATT_CHARACTERISTIC_NAME = BLUEZ_BUS_NAME + ".GattCharacteristic1"
BLUEZ_GATT_DESCRIPTOR_NAME = BLUEZ_BUS_NAME + ".GattDescriptor1"


class BluetoothError(Sleep360Error):

    def __init__(self, msg):
        super().__init__("pydbus", msg)


def get_managed_objects():
    system_bus = SystemBus()
    manager = system_bus.get(BLUEZ_BUS_NAME, "/")
    return manager.GetManagedObjects()


class GattItem:

    def __init__(self, name, bluez_name, uuid, parent_path):
        self._bus = SystemBus()
        self._name = name
        self._path = None

        obj = self._bus.get(BLUEZ_BUS_NAME, "/")
        for (path, values) in obj.GetManagedObjects().items():
            if ((bluez_name in values) and
                (path.startswith(parent_path)) and
                (uuid == values[bluez_name]["UUID"])):
                    self._path = path
                    break

        if self._path == None:
            raise BluetoothError("Can't find the Bluetooth GATT %s %s on the parent %s" % (name, uuid, parent_path))

        try:
            self._obj = self._bus.get(BLUEZ_BUS_NAME, self._path)
        except KeyError:
            raise BluetoothError("Can't create the Bluetooth GATT %s %s" % (self._name, self._path))

    def read(self, options={}):
        return self._obj.ReadValue(options)

    def write(self, value, options={}):
        self._obj.WriteValue(value, options)


class GattDescriptor(GattItem):

    def __init__(self, uuid, gatt_characteristic_path):
        super().__init__("descriptor", BLUEZ_GATT_DESCRIPTOR_NAME, uuid, gatt_characteristic_path)


class GattCharacteristic(GattItem):

    def __init__(self, uuid, gatt_service_path):
        super().__init__("characteristic", BLUEZ_GATT_CHARACTERISTIC_NAME, uuid, gatt_service_path)

    def new_gatt_descriptor(self, uuid):
        return GattDescriptor(uuid, self._path)


class GattService(GattItem):

    def __init__(self, uuid, device_path):
        super().__init__("service", BLUEZ_GATT_SERVICE_NAME, uuid, device_path)

    def new_gatt_characteristic(self, uuid):
        return GattCharacteristic(uuid, self._path)


class Device:

    def __init__(self, device_name=None, device_mac=None, adapter_path=None):
        self._bus = SystemBus()
        self._device_path = None

        if ((device_name == None) and
            (device_mac == None)):
            raise BluetoothError("At least a device name or MAC address must be specified")

        obj = self._bus.get(BLUEZ_BUS_NAME, "/")
        for (path, values) in obj.GetManagedObjects().items():
            if BLUEZ_DEVICE_NAME in values:
                if (device_name and
                    (device_name != values[BLUEZ_DEVICE_NAME]["Alias"])):
                    continue
                if (device_mac and
                    (device_mac.casefold() != values[BLUEZ_DEVICE_NAME]["Address"].casefold())):
                    continue
                if (adapter_path and
                    (adapter_path != values[BLUEZ_DEVICE_NAME]["Adapter"])):
                    continue

                # If the program gets here, the current device matches
                if self._device_path:
                    # If the device has already be seen by another adapter, let's skip it
                    if (self._device_path != values[BLUEZ_DEVICE_NAME]["Adapter"]):
                        continue
                    raise BluetoothError("Several Bluetooth device with the name \"%s\" have been detected" % (values[BLUEZ_DEVICE_NAME]["Alias"]))
                else:
                    self._device_path = path

        if self._device_path == None:
            if (device_mac):
                raise BluetoothError("Can't find a Bluetooth device with the specified MAC address %s" % (device_mac))
            else:
                raise BluetoothError("Can't find a Bluetooth device with the specified name \"%s\"" % (device_name))

        try:
            self._obj = self._bus.get(BLUEZ_BUS_NAME, self._device_path)
        except KeyError:
            raise BluetoothError("Can't create the Bluetooth device %s" % (self._device_path))

    def get_device_name(self):
        return self._obj.Alias

    def get_device_mac(self):
        return self._obj.Address

    def get_adapter_name(self):
        return self._device_path.split("/")[3]

    def connect(self):
        self._obj.Connect()

    def disconnect(self):
        self._obj.Disconnect()

    def is_connected(self):
        return self._obj.Connected

    def is_gatt_ready(self):
        return self._obj.ServicesResolved

    def new_gatt_service(self, uuid):
        return GattService(uuid, self._device_path)


class Adapter:

    def __init__(self, adapter_name=None, adapter_mac=None):
        self._bus = SystemBus()
        self._adapter_path = None

        if adapter_name:
            self._adapter_path = BLUEZ_OBJ_PATH + "/" + adapter_name
        else:
            obj = self._bus.get(BLUEZ_BUS_NAME, "/")
            for (path, values) in obj.GetManagedObjects().items():
                if BLUEZ_ADAPTER_NAME in values:
                    if ((not adapter_mac) or
                        (adapter_mac == values[BLUEZ_ADAPTER_NAME]["Address"])):
                       self._adapter_path = path
                       break
            if self._adapter_path == None:
                if (adapter_mac):
                    raise BluetoothError("Can't find the Bluetooth adapter with the specified MAC address %s" % (adapter_mac))
                else:
                    raise BluetoothError("Can't find a Bluetooth adapter")

        try:
            self._obj = self._bus.get(BLUEZ_BUS_NAME, self._adapter_path)
        except KeyError:
            raise BluetoothError("Can't create the Bluetooth adapter %s" % (self._adapter_path))
        if adapter_mac and (self._obj.Address != adapter_mac):
            raise BluetoothError("The Bluetooth adapter %s does not have the specified MAC address %s" % (adapter_name, adapter_mac))

    def get_adapter_name(self):
        return self._adapter_path.split("/")[3]

    def is_powered(self):
        return self._obj.Powered

    def power_on(self):
        self._obj.Powered = True

    def power_off(self):
        self._obj.Powered = False

    def is_scanning(self):
        return self._obj.Discovering

    def scan_on(self):
        self._obj.StartDiscovery()

    def scan_off(self):
        self._obj.StopDiscovery()

    def new_device(self, device_name=None, device_mac=None):
        return Device(device_name, device_mac, self._adapter_path)
