#!/usr/bin/env python3

import time
import datetime

from sleep360.__about__ import (
    __author__,
    __email__,
    __description__,
    __url__,
    __license__,
    __version__,
)
from sleep360.bluetooth import Adapter
from sleep360.errors import Sleep360Error


name = "sleep360"


class BulbError(Sleep360Error):

    def __init__(self, msg):
        super().__init__(None, msg)


class Bulb:
    """Class used to control a Holi SleepCompanion.

    Exceptions:
    Sleep360Error() -- raised when the Blootooth connection fails
    """

    _adapter = None

    @classmethod
    def init_adapter(self, adapter_pattern=None):
        self._adapter = Adapter(adapter_pattern)
        if not self._adapter.is_powered():
            raise BulbError("Bluetooth adapter is powered off.")

    def __init__(self, name=None, address=None, adapter_pattern=None):
        """Create a new object representing your bulb.

        Note that this constructor must be called with at least one address or one name.

        Keyword arguments:
        address -- MAC address of your SleepCompanion
        name -- Name of your SleepCompanion ("Bulb" by default)
        adapter_pattern -- your Linux bluetooth adapter name (ex. "hci0")
        """
        if self._adapter == None:
            self.init_adapter()
        self._device = self._adapter.new_device(name, address)

    def connect(self):
        """Connects to the bulb (bluetooth LE)."""
        self._device.connect()
        while (not self._device.is_gatt_ready()):
            time.sleep(0.1)
        self._init_gatt_services()

    def _init_gatt_services(self):
        service_hardware = self._device.new_gatt_service("00ff5502-3c25-45cb-99dc-1754766b829a")
        service_application = self._device.new_gatt_service("01ff5502-3c25-45cb-99dc-1754766b829a")
        service_bootloader = self._device.new_gatt_service("02ff5502-3c25-45cb-99dc-1754766b829a")

        self._chrc_off =            service_hardware.new_gatt_characteristic("044993e6-5eed-439a-9497-9e4086539756")
        self._chrc_temp =           service_hardware.new_gatt_characteristic("0b4993e6-5eed-439a-9497-9e4086539756")

        self._chrc_desc_light =     service_application.new_gatt_characteristic("f04993e6-5eed-439a-9497-9e4086539756")
        self._chrc_segment_light =  service_application.new_gatt_characteristic("f14993e6-5eed-439a-9497-9e4086539756")

    def disconnect(self):
        self._device.disconnect()

    def set_color(self, red, green, blue, warm, cold):
        """Turn ON the light if not already ON and set the given color.

        Arguments:
        red, green, blue -- RGB values (0x00 to 0xff)
        warm -- adjust the warmth of the light (0x00 to 0xff)
        cold -- adjust the coldness of the light (0x00 to 0xff)
        """
        self._chrc_desc_light.write([0x00, 0x00, 0x01, 0x00])
        self._chrc_segment_light.write([0x00, 0x00, 0x00, 0x00, 0x00,
                                        red & 0xff, green & 0xff, blue & 0xff,
                                        warm & 0xff, cold & 0xff])

    def off(self):
        """Turn OFF the light."""
        self._chrc_off.write([0x00])

    def get_temperatures(self):
        """Retrieve temperatures from the bulb and return them.

        Return:
        a dict where keys are datetime.datetime objects
                     values are temperatures (float) given in Celsius
        """
        temp_map = {}
        temp_bytes = self._chrc_temp.read()
        i = 0
        while (i + 6) < len(temp_bytes):
            timestamp = ((temp_bytes[i + 3] << 24) |
                         (temp_bytes[i + 2] << 16) |
                         (temp_bytes[i + 1] << 8) |
                         (temp_bytes[i]))
            temperature = ((temp_bytes[i + 5] << 8) |
                           temp_bytes[i + 4]) / 10.0
            if timestamp:
                timestamp -= time.localtime().tm_gmtoff
                temp_map[datetime.datetime.fromtimestamp(timestamp)] = temperature
            i += 6
        return temp_map
