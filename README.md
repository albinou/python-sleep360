# Sleep360

This project allows to control the Holi SleepCompanion from a Linux computer.

The SleepCompanion is nice LED bulb that offers a lot of features to improve your sleep and your waking up.
It is manufactured by Holi, a French company.
A product description can be found by following these links:

- [Official Website (French)](https://www.holi.io/?portfolio=sleepcompanion-lampoule-qui-vous-reveille-les-neurones)
- [An English overview](https://www.hobbr.com/holi-sleep-companion)

This Sleep360 project is written in Python and can be used:

- as a command line utility
- within a Python program

## Requirements

- D-BUS
- bluez >= 5.46 (dependency on the GATT D-BUS API)

## Installation

### From PyPI

    pip install [--user] sleep360

### From the sources

    cd python-sleep360
    pip install [--user] .

## Command line usage

`pip install` installs a `sleep360` binary.
Running it gives a prompt like this one:

    Sleep360 shell (version x.y.z).
    Type help or ? to list commands.

    [sleep360] connect Bulb
    Connecting to the Bulb...
    [sleep360] set_color 0xff0000 0x00 0x00
    [sleep360] disconnect
    Disconnecting from the Bulb...
    [sleep360] exit

## Python module usage

    bulb = Bulb(name="Bulb")
    bulb.connect()

    # Play with bulb commands
    [...]

    bulb.disconnect()

Examples are available in the [tests](tests) directory.

## Supported features

The SleepCompanion bulb offers a lot of features.
Among them, the following ones are currently supported by Sleep360:

- Setting colors (RGB + warm and cold value)
- Powering off the light
- Getting the temperatures measured by the bulb

## Testing & Issues

I only own one SleepCompanion.
Hence, please share with me any issue you could encounter with your own device.
Issue can be created [here](https://gitlab.com/albinou/python-sleep360/issues).

## Reverse-engineering

This project is developed by carefully studying the Bluetooth LE messages.
The file [BLE-protocol.md](BLE-protocol.md) describes my understanding of these messages.
