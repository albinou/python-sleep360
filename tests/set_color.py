#!/usr/bin/env python3

import sys
import time

from sleep360 import Bulb


def main():
    bulb = Bulb(name="Bulb")
    bulb.connect()

    # Turn on the light and set its color to red
    bulb.set_color(0xff, 0x00, 0x00, 0x00, 0x00)
    time.sleep(0.5)
    # Change the bulb color to green
    bulb.set_color(0x00, 0xff, 0x00, 0x00, 0x00)
    time.sleep(0.5)
    # Change the bulb color to blue
    bulb.set_color(0x00, 0x00, 0xff, 0x00, 0x00)
    time.sleep(0.5)

    # Turn off the light
    bulb.off()

    bulb.disconnect()
    return 0


if __name__ == '__main__':
    sys.exit(main())
