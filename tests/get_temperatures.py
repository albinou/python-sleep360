#!/usr/bin/env python3

import sys

from sleep360 import Bulb


def main():
    bulb = Bulb(name="Bulb")
    bulb.connect()

    temp_map = bulb.get_temperatures()
    print("Temperatures:")
    for (d, t) in temp_map.items():
        print("  - %s: %.2f℃" % (d, t))

    bulb.disconnect()
    return 0


if __name__ == '__main__':
    sys.exit(main())
